# Página para Festival de música
* Landing page (index.html)
* Galería incluida

## Creado con
* HTML5
* CSS3
* JavaScript
* SCSS
* Gulp

URL: [EDM Music Festival](https://music-festival-edm.netlify.app/)
