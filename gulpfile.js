const { src, dest, watch, parallel } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const postcss = require("gulp-postcss");
const sourcemaps = require("gulp-sourcemaps");

// Img webp
const cache = require("gulp-cache");
const imagemin = require("gulp-imagemin");
const imgMin = require("gulp-imagemin");
const webp = require("gulp-webp");
const avif = require("gulp-avif");

const terser = require("gulp-terser-js");

function css(cb) {
  /**
   * 1.Identificar archivo sass
   * 2.Compilarlo
   * 3.Almacenarla en el disco duro
   */

  src("src/scss/app.scss")
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write("."))
    .pipe(dest("build/css"));

  cb(); // Funcion callback que avisa cuando se llego al final de la tarea
}

function imgs(cb) {
  const op = {
    optimizationLevel: 3,
  };

  src("src/img/**/*.{png,jpg}")
    .pipe(cache(imagemin(op)))
    .pipe(dest("build/img"));
  cb();
}

function webpVer(cb) {
  const op = {
    quality: 50,
  };

  src("src/img/**/*.{png,jpg}").pipe(webp(op)).pipe(dest("build/img"));
  cb();
}

function avifVer(cb) {
  const op = {
    quality: 50,
  };

  src("src/img/**/*.{png,jpg}").pipe(avif(op)).pipe(dest("build/img"));
  cb();
}

function javascript(cb) {
  src("src/js/**/*.js")
    .pipe(sourcemaps.init())
    .pipe(terser())
    .pipe(sourcemaps.write("."))
    .pipe(dest("build/js"));
  cb();
}

function dev(cb) {
  watch("src/scss/**/*.scss", css); // vigila los cambios
  watch("src/js/**/*.js", javascript); // vigila los cambios
  cb();
}

exports.css = css;
exports.js = javascript;
exports.imgs = parallel(imgs, webpVer, avifVer);
exports.webpVer = webpVer;
exports.avifVer = avifVer;
// ejecutar diferentes tareas al mismo tiempo
// exports.dev = parallel( webpVer, dev);
exports.dev = parallel(javascript, dev); // npx gulp dev
