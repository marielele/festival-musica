document.addEventListener("DOMContentLoaded", function () {
  startApp();
});

function startApp() {
  fixedNav();
  createGallery();
  scrollNav();
}

function fixedNav() {
  const navbar = document.querySelector(".header");
  const festInfo = document.querySelector(".about-festival");
  const body = document.querySelector("body");

  window.addEventListener("scroll", function () {
    if (festInfo.getBoundingClientRect().top < 0) {
      navbar.classList.add("fixed");
      body.classList.add("body-scroll"); // soluciona salto de linea del body al mover la navbar
    } else {
      navbar.classList.remove("fixed");
      body.classList.remove("body-scroll");
    }
  });
}

function scrollNav() {
  const links = document.querySelectorAll(".main-nav a");
  links.forEach((link) => {
    link.addEventListener("click", function (e) {
      e.preventDefault();
      const sectionScroll = e.target.attributes.href.value;
      const section = document.querySelector(sectionScroll);
      section.scrollIntoView({ behavior: "smooth" });
    });
  });
}

function createGallery() {
  const gallery = document.querySelector(".gallery-imgs");
  for (let i = 1; i <= 12; i++) {
    const image = document.createElement("picture");
    image.innerHTML = `
    <source srcset="build/img/thumb/${i}.avif" type="image/avif" />
    <source srcset="build/img/thumb/${i}.webp" type="image/webp" />
    <img
      loading="lazy"
      width="200"
      height="300"
      src="build/img/thumb/${i}.jpg"
      alt="Imagen galeria no.${i}"
    />
    `;
    image.onclick = function () {
      showImage(i);
    };
    gallery.appendChild(image);
  }
}

function showImage(index) {
  const image = document.createElement("picture");
  image.innerHTML = `
      <source srcset="build/img/grande/${index}.avif" type="image/avif" />
      <source srcset="build/img/grande/${index}.webp" type="image/webp" />
      <img
        loading="lazy"
        width="200"
        height="300"
        src="build/img/grande/${index}.jpg"
        alt="Imagen galeria no.${index}"
      />
      `;
  image.classList.add("xl-pic");
  const overlay = document.createElement("div");
  overlay.appendChild(image);
  overlay.classList.add("overlay");

  const btnClose = document.createElement("p");
  btnClose.textContent = "X";
  btnClose.classList.add("btn-close");
  btnClose.onclick = function () {
    overlay.remove();
    const body = document.querySelector("body");
    body.classList.remove("lock-body");
  };
  overlay.appendChild(btnClose);

  const body = document.querySelector("body");
  body.appendChild(overlay);
  body.classList.add("lock-body");
}
